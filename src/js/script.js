const button = document.querySelector(".button");
const activeButton = document.querySelector(".active_button");
const navmenu = document.querySelector(".navmenu")


button.addEventListener("click", () => {
    button.style.display = "none";
        activeButton.style.display = "block";
        navmenu.classList.add("active"); 
});
activeButton.addEventListener("click", ()=>{
     button.style.display = "block";
    activeButton.style.display = "none";
    navmenu.classList.remove("active"); 
})
